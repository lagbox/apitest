<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'auth/provider', 'middleware' => 'auth'], function () {
    Route::get('test', '\API\Controllers\OAuthController@test');
    Route::get('testgh', '\API\Controllers\OAuthController@testgh');
    Route::get('{name}', '\API\Controllers\OAuthController@redirectToProvider');
    Route::get('{name}/callback', '\API\Controllers\OAuthController@providerCallback');
});

Route::auth();

Route::get('/home', 'HomeController@index');
