<?php

namespace App\Http\Controllers;

use API\ApiManager;
use Illuminate\Http\Request;

class OAuthController extends Controller
{
    public function __construct(ApiManager $manager)
    {
        $this->manager = $manager;
    }

    public function redirectToProvider($name)
    {
        $provider = $this->manager->driver($name);

        dd($provider);

        return $provider->authorize();
    }

    public function providerCallback($name)
    {
        $provider = $this->manager->driver($name);


    }
}
