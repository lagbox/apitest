# Laravel PHP Framework

Guzzle Web Service Client class

* OAuth1 - verified using twitter
  * doesn't seem to require the RSA signature
* OAuth2 - verified via github
  * How to handle refresh tokens
    * github doesn't provide them so can't test with github