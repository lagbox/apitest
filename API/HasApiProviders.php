<?php

namespace API;

interface HasApiProviders
{
    public function providers();

    public function hasProvider($provider);

    public function getProvider($provider);

    public function addProvider($provider, $data);

    public function updateProvider($provider, $data);
}
