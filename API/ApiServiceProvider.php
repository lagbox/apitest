<?php

namespace API;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->singleton(ApiManager::class, function ($app) {
            return new ApiManager($app);
        });

        $this->app->alias(ApiManager::class, 'api');
    }
}