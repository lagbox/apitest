<?php

namespace API;

use Psr\Http\Message\RequestInterface;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

use Sainsburys\Guzzle\Oauth2\GrantType\RefreshToken;
use Sainsburys\Guzzle\Oauth2\GrantType\AuthorizationCode;
use Sainsburys\Guzzle\Oauth2\Middleware\OAuthMiddleware;

class OAuthTwo extends Provider
{
    public function newClient()
    {
        $stack = HandlerStack::create();

        $client = new Client([
            'handler' => $stack,
            'base_uri' => $this->config['base_uri'],
            'auth' => 'oauth2',
        ]);

        $config = [
            // 'redirect_uri' => $this->config['redirect'],
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
            // 'scopes' => ...,
        ];


        $providerData = $this->getProviderDataForUser();

        $config = $config + $providerData;


        // $token = new AuthorizationCode($client, $config);
        $refreshToken = new RefreshToken($client, $config);
        // $middleware = new OAuthMiddleware($client, $token, $refreshToken);

        $middleware = new OAuthMiddleware($client, $refreshToken, $refreshToken);

        $userToken = $this->userProvider->token;
        $expiresIn = $this->userProvider->expires_in;

        $middleware->setAccessToken($userToken, 'bearer', $expiresIn);

        $stack->push($middleware->onBefore());
        $stack->push($middleware->onFailure(5));


        // $m = function (callable $handler) use ($userToken) {
        //     return function (RequestInterface $request, array $options) use ($handler, $userToken) {
        //         foreach ($this->extraHeaders() as $key => $value) {
        //             $request->withAddedHeader($key, $value);
        //         }

        //         return $handler(
        //             $request->withAddedHeader('Authorization', 'Bearer '. $userToken),
        //             $options
        //         );
        //     };
        // };

        // $stack->push($m);

        return $client;
    }

    function getProviderDataForUser()
    {
        // if (! $provider = $this->userProvider) {
        //     if (! $provider = $this->user->getProvider($this->name)) {
        //         return false;
        //     }
        // }

        $provider = $this->userProvider;

        return [
            'token' => $provider->token,
            'refresh_token' => $provider->refresh_token,
            'expires_in' => $provider->expires_in,
        ];
    }

    public function providersRecord($ouser)
    {
        return [
            'provider' => $this->name,
            'token' => $ouser->token,
            'refresh_token' => $ouser->refreshToken,
            'expires_in' => $ouser->expiresIn,
        ];
    }
}
