<?php

namespace API\Controllers;

use API\ApiManager;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class OAuthController extends Controller
{
    public function __construct(ApiManager $manager)
    {
        $this->manager = $manager;
    }

    public function redirectToProvider($name)
    {
        $provider = $this->manager->driver($name);

        return $provider->authorize();
    }

    public function providerCallback(Request $request, $name)
    {
        $provider = $this->manager->driver($name);

        $user = $request->user();

        $provider->forUser($user);

        $r = $provider->authorizeCallback();

        dd($r);
    }

    public function test(Request $request)
    {
        $provider = $this->manager->driver('twitter');

        $user = $request->user();

        $provider->forUser($user);

        dd($provider->newClient());

        $r = $provider->get('account/verify_credentials.json');

        dd($r);
    }

    public function testgh(Request $request)
    {
        $provider = $this->manager->driver('github');

        $user = $request->user();

        $provider->forUser($user);

        $r = $provider->get('user');

        dd($r);
    }
}
