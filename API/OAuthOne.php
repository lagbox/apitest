<?php

namespace API;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class OAuthOne extends Provider
{
    protected function verifyConfig($config)
    {
        // $required = [
        //     'client_id',
        //     'client_secret',
        //     'base_uri',
        // ];

        // $intersect = array_intersect($required, array_filter($config));

        // if (count($intersect) != count($required)) {
        //     throw new \Exception('Missing config values.');
        // }
    }

    public function newClient()
    {
        $stack = HandlerStack::create();

        if (! $providerData = $this->getProviderDataForUser()) {
            // no provider for this user
        }

        $basics = [
            'consumer_key' => $this->config['client_id'],
            'consumer_secret' => $this->config['client_secret'],
        ];

        $signature = $this->config['signature'] ?? null;

        if ($signature && $signature == 'RSA') {
            $basics =  $basics + [
                'private_key_file' => config('apis.signature.private_key_file'),
                'private_key_passphrase' => config('apis.signature.private_key_passphrase'),
                'signature_method' => Oauth1::SIGNATURE_METHOD_RSA,
            ];
        }

        $middleware = new Oauth1($basics + $providerData);

        $stack->push($middleware);

        $client = new Client([
            'base_uri' => $this->config['base_uri'],
            'handler' => $stack,
            'auth' => 'oauth',
        ]);

        return $client;
    }

    function getProviderDataForUser()
    {
        if (! $provider = $this->user->getProvider($this->name)) {
            return false;
        }

        return [
            'token' => $provider->token,
            'token_secret' => $provider->token_secret,
        ];
    }

    public function providersRecord($ouser)
    {
        return [
            'provider' => $this->name,
            'token' => $ouser->token,
            'token_secret' => $ouser->tokenSecret,
        ];
    }
}
