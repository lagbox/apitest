<?php

namespace API\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProvider extends Model
{
    protected $fillable = [
        'provider', 'token', 'token_secret', 'refresh_token', 'expires',
    ];

    public function user()
    {
        $userClass = config('apis.user.model');

        return $this->belongsTo($userClass);
    }
}
