<?php

namespace API;

abstract class Provider
{
    // guzzle client
    protected $client;

    // config array for provider
    protected $config;

    // socialite driver
    protected $driver;

    // HasApiProviders user
    protected $user;

    // want results json_decoded
    protected $json = true;

    // the HasApiProviders user's provider record for this provider
    protected $userProvider;

    /**
     * @param array $config
     * @param \Laravel\Socialite\Contracts\Provider $driver socialite driver
     * @param string $name   name of the current provider
     */
    public function __construct($config, $driver, $name)
    {
        $this->verifyConfig($config);

        $this->config = $config;
        $this->driver = $driver;
        $this->name = $name;
    }

    /**
     * Get the guzzle client
     *
     * @return \GuzzleHttp\Client
     */
    public function getClient()
    {
        if (! $this->user) {
            throw new \Exception('No user set for provider.');
        }

        if (! $this->client) {
            $this->client = $this->newClient();
        }

        return $this->client;
    }

    /**
     * Set the user for the provider
     *
     * @param  HasApiProviders $user
     * @return void
     */
    public function setUser(HasApiProviders $user)
    {
        $this->user = $user;
    }

    /**
     * Set and setup provider for the user
     *
     * @param  HasApiProviders $user [description]
     * @return void
     */
    public function forUser(HasApiProviders $user)
    {
        $this->setUser($user);

        $userProvider = $user->getProvider($this->name);

        if ($userProvider) {
            $this->userProvider = $userProvider;
        }
    }

    /**
     * Get a new Guzzle Client setup with the correct stack handler
     *
     * @return \GuzzleHttp\Client
     */
    abstract public function newClient();

    /**
     * Create the authorization redirect
     *
     * @param  array|null $params The extra parameters to pass with the redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authorize($params = null)
    {
        if (isset($this->config['scopes'])) {
            $this->driver->scopes($this->config['scopes']);
        }

        if ($params) {
            $this->driver->with($params);
        }

        return $this->driver->redirect();
    }

    /**
     * Handle the authorization callback from the socialite provider
     *
     * @return \Laravel\Socialite\Contracts\User
     */
    public function authorizeCallback()
    {
        $user = $this->driver->user();

        $providerData = $this->providersRecord($user);

        //if ($this->user->hasProvider($this->name)) {
        if ($this->userProvider) {
            $this->user->updateProvider($this->name, $providerData);
        } else {
            $this->user->addProvider($this->name, $providerData);
        }

        return $user;
    }

    /**
     * Make a request via the guzzle client
     *
     * @param  string $method The HTTP method
     * @param  string $uri    The URI endpoint
     * @param  array  $params Parameters to send with the request
     * @return \stdClass
     */
    public function call($method, $uri, $params = [])
    {
        if (! $this->client) {
            $this->getClient();
        }

        $response = $this->client->{$method}($uri, $params);

        $contents = $response->getBody()->getContents();

        if ($this->json) {
            $contents = json_decode($contents);
        }

        return $contents;
    }

    public function __call($method, $args)
    {
        $methods = ['get', 'post', 'put', 'patch', 'delete', 'head'];

        $method = strtolower($method);

        if (in_array($method, $methods)) {
            return $this->call($method, ...$args);
        }

        return false;
    }

    protected function extraHeaders()
    {
        return $this->config['headers'] ?? [];
    }
}
