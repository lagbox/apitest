<?php

namespace API;

use Illuminate\Support\Str;
use Illuminate\Support\Manager;

class ApiManager extends Manager
{
    // App\User or interface
    protected $user;

    protected $oauth = [
        'oauth1' => 'API\OAuthOne',
        'oauth2' => 'API\OAuthTwo',
    ];

    protected function createTwitterDriver()
    {
        $config = $this->app['config']['services.twitter'];

        $class = $this->oauth[$config['auth']];

        return new $class(
            $config,
            $this->app['Laravel\Socialite\Contracts\Factory']->driver('twitter'),
            'twitter'
        );
    }

    public function getDefaultDriver()
    {
        return 'twitter';
    }

    protected function createDriver($driver)
    {
        $method = 'create'.Str::studly($driver).'Driver';

        // We'll check to see if a creator method exists for the given driver. If not we
        // will check for a custom driver creator, which allows developers to create
        // drivers using their own customized driver creator Closure to create it.
        if (isset($this->customCreators[$driver])) {
            return $this->callCustomCreator($driver);
        } elseif (method_exists($this, $method)) {
            return $this->$method();
        } else {
            if ($driver = $this->buildDriver($driver)) {
                return $driver;
            }
        }

        throw new InvalidArgumentException("Driver [$driver] not supported.");
    }

    protected function buildDriver($name)
    {
        if ($config = $this->app['config']->get('services.'. $name)) {
            $class = $this->oauth[$config['auth']];

            return new $class(
                $config,
                $this->app['Laravel\Socialite\Contracts\Factory']->driver($name),
                $name
            );
        }
    }
}


