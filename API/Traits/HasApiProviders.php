<?php

namespace API\Traits;

use API\Models\UserProvider as Provider;

trait HasApiProviders
{
    /**
     * Providers relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providers()
    {
        return $this->hasMany(Provider::class);
    }

    /**
     * Does the user have the named provider record
     *
     * @param  string  $provider The API provider's name
     * @return boolean
     */
    public function hasProvider($provider)
    {
        return $this->providers()
            ->where('provider', $provider)
            ->exists();
    }

    /**
     * Get the named provider for the user
     *
     * @param  string $provider The API provider's name
     * @return UserProvider|null
     */
    public function getProvider($provider)
    {
        return $this->providers()
            ->where('provider', $provider)
            ->first();
    }

    /**
     * Add a provider record for the user
     *
     * @param string $provider The API provider's name
     * @param bool $data
     */
    public function addProvider($provider, $data)
    {
        $r = $this->providers()
            ->create(['provider' => $provider] + $data);

        return (bool) $r->exists;
    }

    /**
     * Update a provider record for the user
     * @param  string $provider The API provider's name
     * @param  array $data
     * @return bool
     */
    public function updateProvider($provider, $data)
    {
        $provider = $this->providers()
            ->where('provider', $provider)
            ->first();

        if ($provider) {
            return $provider->update($data);
        }

        return false;
    }
}
