<?php

return [

    'user' => [
        'model' => 'App\User'
    ],

    'signature' => [
        'private_key_file' => '',
        'private_key_phrase' => '',
    ],

];
